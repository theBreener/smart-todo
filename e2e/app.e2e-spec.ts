import { SmartTodoPage } from './app.po';

describe('smart-todo App', () => {
  let page: SmartTodoPage;

  beforeEach(() => {
    page = new SmartTodoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
