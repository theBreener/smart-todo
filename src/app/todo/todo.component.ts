import { Component, OnInit } from '@angular/core';
import {TodoService} from "./shared/todo.service";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
  providers: [TodoService]
})
export class TodoComponent implements OnInit {
  toDolistArray:any[];
  constructor(private toDoService: TodoService) { }

  ngOnInit() {
    this.toDoService.getTodoList().snapshotChanges()
      .subscribe(item =>{
        this.toDolistArray =[];
        item.forEach(element =>{
          let todo = element.payload.toJSON();
          todo["$key"] = element.key;
          this.toDolistArray.push(todo);
        });
        this.toDolistArray.sort((a,b) =>{
          return a.finished - b.finished;
        })
      });
  }
  onAdd(todoTitle){
    this.toDoService.addTodo(todoTitle.value);
      todoTitle.value = null;
  }
  toggleFinished($key: string, toggleFinish){
    this.toDoService.toggleFinished($key, !toggleFinish);
  }
  deleteTodo($key:string){
    this.toDoService.removeTodo($key);
  }
}
