import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from "angularfire2/database";

@Injectable()
export class TodoService {
  toDoList: AngularFireList<any>;
  constructor(private firebasedb: AngularFireDatabase) { }

  /*henter toDolisten fra firebase*/
  getTodoList(){
    this.toDoList = this.firebasedb.list('todos');
    return this.toDoList;
  }
  addTodo(todo: string){
    this.toDoList.push({
      title: todo,
      finished: false
    });
  }
  removeTodo($key: string){
    this.toDoList.remove($key);
  }
  toggleFinished($key:string, toggle:boolean){
    this.toDoList.update($key,{finished: toggle});
  }
}
